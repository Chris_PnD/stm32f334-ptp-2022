# pip install pyserial
# pip install matplotlib

import threading
import serial.tools.list_ports
import time
import datetime
import re
import collections
import struct
import numpy
import http.server
import socketserver
import random
import json
import cgi

from tkinter import *
from tkinter import ttk
from _datetime import date
from typing import List, Any
from matplotlib.backends.backend_tkagg import (FigureCanvas)
from matplotlib.figure import Figure
from matplotlib.ticker import FuncFormatter
from matplotlib.dates import num2date
from numpy import argmax, mean, sqrt, square
from _overlapped import NULL
from urllib import parse

MAX_SIZE_OF_DATA_BUFFER = 200

SLIP_END = 0x0A  # '/n' 
SLIP_ESC = 0xDB
SLIP_ESC_END = 0xDC
SLIP_ESC_ESC = 0xDD

class WebServerhandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/index.html' or self.path == '/':
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            f = open("index3.html", "rb")
            self.wfile.write(f.read())
            f.close()
            
        if self.path == '/favicon.ico':
            self.send_response(200)
            self.send_header('Content-type', 'image/X-icon')
            self.end_headers()
            f = open("favicon.ico", "rb")
            self.wfile.write(f.read())
            f.close()
            
        if self.path == '/event':
            self.send_response(200)
            self.send_header('Content-type', 'text/event-stream')
            self.send_header("Connection", "keep-alive")
            self.end_headers()
            sse_socket_list.append(self.connection)    

    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers['content-type'])
        
        # refuse to receive non-json content
        if ctype != 'application/json':
            self.send_response(400)
            self.end_headers()
            return
            
        # read the message and convert it into a python dictionary
        length = int(self.headers['content-length'])
        jsonMessage = json.loads(self.rfile.read(length))
        adcFreqVal = jsonMessage.get('adcFreq')
        combobox_adcFreq.set(adcFreqVal +' Hz')
        _set_adc_frequency()
                    
                
class ThreadingSimpleServer(socketserver.ThreadingMixIn, http.server.HTTPServer):
    pass

def server_forever(httpd):
    with httpd:
        httpd.serve_forever()

def webserver_init():
    address = ('', 80)
    httpd = ThreadingSimpleServer(address, WebServerhandler)

    # set server forever thread
    thread = threading.Thread(target=server_forever, args=(httpd,))
    thread.setDaemon(True)
    thread.start()

def decodeSLIP(serialBytes):
    dataBuffer = bytearray()
    escaped = False
    for serialByte in serialBytes:
        
        if serialByte == SLIP_END: 
            if len(dataBuffer) > 0:  
                return dataBuffer
            else:
                return None
            
        elif serialByte == SLIP_ESC:
            escaped = True
            
        elif serialByte == SLIP_ESC_END:
            if escaped == True:
                dataBuffer.append(SLIP_END)
                escaped = False
            else:
                dataBuffer.append(serialByte)    

        elif serialByte == SLIP_ESC_ESC:
            if escaped == True:
                dataBuffer.append(SLIP_ESC)
                escaped = False
            else:
                dataBuffer.append(serialByte)    
        
        else:
            dataBuffer.append(serialByte)
            
    return None


def read_uint12(data_chunk):
    data = numpy.frombuffer(data_chunk, dtype=numpy.uint8)
    fst_uint8, mid_uint8, lst_uint8 = numpy.reshape(data, (data.shape[0] // 3, 3)).astype(numpy.uint16).T
    
    fst_uint12 = (fst_uint8 << 4) + (mid_uint8 >> 4)
    snd_uint12 = ((mid_uint8 % 16) << 8) + lst_uint8
    return numpy.reshape(numpy.concatenate((fst_uint12[:, None], snd_uint12[:, None]), axis=1), 2 * fst_uint12.shape[0])

def send_info_to_web_front(string):
    for socket in sse_socket_list:
        try:
            id = "nodeInfo"
            sseJSON = 'data: {"type": "text", "id": "%s", "val": "%s"}\n\n' % (id, string)
            socket.send(sseJSON.encode('utf-8'))
        except:
            socket.close()
            sse_socket_list.remove(socket)
            print("connection close by client")


def uart_read_thread(s):
    # serial data read
    while True:
        thread_pause.wait()
        data_append = False
        sub_x: List[Any] = []
        sub_y: List[Any] = []
        # serial data read
        serialLine = s.readline()
        if serialLine[0:4] == bytes('Data', 'utf-8'):
            serial_string_line = serialLine.decode('utf-8')
            # print(serial_string_line[0:-2])
            # decode data from printed serial line - use https://regex101.com/ to parse
            regex = r"-?\d*\.\d+|\d+"
            matches = re.finditer(regex, serial_string_line)
            for matchNum, match in enumerate(matches, start=1):
                # parse date
                if matchNum == 1:
                    hour = int(match[0])
                if matchNum == 2:
                    minute = int(match[0])
                if matchNum == 3:
                    second = float(match[0])
    
                # parse data
                if matchNum == 4:
                    todays_date = date.today()
                    today_year = todays_date.year
                    today_month = todays_date.month
                    today_day = todays_date.day
                    sec = int(second)
                    msec = int((second - sec) * 1000000)
                    x_time = datetime.datetime(today_year, today_month, today_day,
                                               hour, minute, sec, msec)
                    y_float = float(match[0])
                    sub_x.append(x_time)
                    sub_y.append(y_float)
                    data_append = True
            # print(hour, minute, second)    
        elif serialLine[0:2] == bytes("B:", 'utf-8'):
            serialBinData = serialLine[2:]
            # print("ENCODED:" + serialBinData.hex())
            decodedLine = decodeSLIP(serialBinData)
            if len(decodedLine) == 12:
                # print("DECODED:" + decodedLine.hex())
                epoch = struct.unpack('d', decodedLine[0:8])[0]
                adc_value = struct.unpack('f', decodedLine[8:12])[0]
                x_time = datetime.datetime.fromtimestamp(epoch)
                sub_x.append(x_time)
                sub_y.append(adc_value)
                data_append = True
                # print("Bin Time:" + str(x_time) + " value:" + str(adc_value))
                
        elif serialLine[-4:-2] == bytes("P:", 'utf-8'):
            # read number of bytes in packet
            size_of_packed_buffer = ord(serialLine[-2:-1])
            # print("PACKET: "+ str(size_of_packed_buffer) + " bytes")
            # create empty byte array
            packed_data = bytearray(0)
            # read number of packed data adn add it to list
            for packed_byte in range(size_of_packed_buffer):
                packed_data.append(ord(s.read(1)))
            # print(packed_data)
            frequency = struct.unpack('H', packed_data[0:2])[0]                           
            epoch = struct.unpack('d', packed_data[2:10])[0]
            # print("PACKET -> time:" + str(datetime.datetime.fromtimestamp(epoch)) + " size:" + str(size_of_packed_buffer) + " freq:" + str(frequency));
            packed_adc_data = packed_data[10:]
            unpacked_data = read_uint12(packed_data[10:])
            # print(unpacked_data)
            try:
                for (data, i) in zip(unpacked_data, range(len(unpacked_data))):
                    y_float = 3.3 * data / 0xFFF
                    # print(i, adc_raw_value)
                    x_time = datetime.datetime.fromtimestamp(epoch + i * (1/frequency))
                    sub_x.append(x_time)  
                    sub_y.append(y_float)
                    # print(x_time, y_float)
                    data_append = True;
            except:
                print("parser bin packed warning")
                
        elif serialLine[:4] == bytes("ACK:", 'utf-8'):
            # TODO: validate comman
            print("Respond from nucleo:" + serialLine[:-1].decode('utf-8'))
            send_info_to_web_front(serialLine[:-1].decode('utf-8'))
            
        if data_append == True:
            for socket in sse_socket_list:
                try:
                    # send data to web front
                    for (x_time, y_float) in zip(sub_x, sub_y):
                        time_ms = x_time.replace(tzinfo=datetime.timezone.utc).timestamp() * 1000
                        Vout = y_float
                        name = 'Nucleo ADC channel A0'
                        sseJSON = 'data: {"type": "chart", "name": "%s", "x": %.3f, "y": %.3f}\n\n' % (name, time_ms, Vout)
                        socket.send(sseJSON.encode('utf-8'))
                except:
                    socket.close()
                    sse_socket_list.remove(socket)
                    print("connection close by client")
            # add data to python display
            x.extend(sub_x)
            y.extend(sub_y)  

def formatter(a, b):
    t = num2date(a)
    ms = str(t.microsecond)[:1]
    res = f"{t.hour:02}:{t.minute:02}:{t.second:02}.{ms}"
    return res


def refresh_thread(s):
    while True:
        thread_pause.wait
        if len(y) > 0:
            averageVoltage = str(float(f'{mean(y):.2f}'))
            rms = str(float(f'{sqrt(mean(square(y))):.2f}'))
        else:
            averageVoltage = '---'
            rms = '---'

        dataPlot.clear()
        dataPlot.plot(x, y, "-o", color='blue')
        dataPlot.grid()
        dataPlot.set_title('ADC average voltage: ' + averageVoltage + ' [V]  RMS:' + rms)
        dataPlot.set_xlabel('time [sec]')
        dataPlot.set_ylabel('voltage [V]')
        dataPlot.xaxis.set_major_formatter(FuncFormatter(formatter))
        figData.canvas.draw()


def _quit():
    root.quit()  # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent


def _clear_data():
    x.clear()
    y.clear()
    figData.canvas.draw()


def _set_adc_frequency():
    if s.isOpen():
        command = 'adcf ' + str(combobox_adcFreq.get()[:-3]) + '\r'
        # print(command)
        s.write(command.encode())


def _set_dac_frequency():
    if s.isOpen():
        command = 'dacf ' + str(combobox_dacFreq.get()[:-3]) + '\r'
        # print(command)
        s.write(command.encode())


def _open_port(thread1, thread2):
    if not s.isOpen():
        print(datetime.datetime.now())
        s.port = combobox_port.get()
        if s.port != "":
            s.open()
            print("Serial port: " + s.port + " opened")

            # send time info to stm32
            epoch = int(time.time())
            command = 'time ' + str(epoch) + '\r'
            print(command)
            s.write(command.encode())

            # start read from serial port in seperated thred
            if not thread1.is_alive():
                thread1.start()
            if not thread2.is_alive():
                thread2.start()

            thread_pause.set()
        else:
            print("Can NOT open serial port")
    else:
        thread_pause.clear()
        time.sleep(0.5)
        s.close()


x = collections.deque(maxlen=MAX_SIZE_OF_DATA_BUFFER)
y = collections.deque(maxlen=MAX_SIZE_OF_DATA_BUFFER)

root = Tk()
root.wm_title("STM32 data viewer")
# root.geometry("700x400")

# setup frame
frameData = Frame(root)
frameFft = Frame(root)
frameOpen = Frame(root)
frameCtl = Frame(root)
frameClose = Frame(root)

frameData.pack(side=TOP, fill=BOTH, expand=1)
frameFft.pack(side=TOP, fill=BOTH, expand=1)
frameCtl.pack(side=BOTTOM, fill=BOTH)

# assign serial 
s = serial.Serial(timeout=10)
s.baudrate = 38400

# assign read deamon thread
thread_uart_read = threading.Thread(target=uart_read_thread, args=(s,))
thread_uart_read.setDaemon(True)
# assign canvas refresh deamon thread
thread_refresh = threading.Thread(target=refresh_thread, args=(s,))
thread_refresh.setDaemon(True)
thread_pause = threading.Event()  # The flag used to pause the thread

# placeing figures
figData = Figure(figsize=(10, 5), dpi=70)
dataPlot = figData.add_subplot(111)

canvasData = FigureCanvas(figData, master=frameData)
canvasData.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)

# add button to canvas
button_open = ttk.Button(master=frameCtl, text="Open & Read / Stop",
                         command=lambda: _open_port(thread_uart_read, thread_refresh))
button_quit = ttk.Button(master=frameCtl, text="Quit", command=_quit)
button_clear = ttk.Button(master=frameCtl, text="Clear Data", command=_clear_data, width=10)

# add combobox with available port
combobox_port = ttk.Combobox(master=frameCtl, width=6)

# add combox
combobox_adcFreq = ttk.Combobox(master=frameCtl, values=["1 Hz", "10 Hz", "20 Hz", "50 Hz", "100 Hz", "200 Hz", "500 Hz", "1000 Hz", "2000 Hz"], width=8)
combobox_dacFreq = ttk.Combobox(master=frameCtl, values=["1 Hz", "10 Hz", "20 Hz", "50 Hz", "100 Hz", "200 Hz"], width=8)
combobox_adcFreq.set("1 Hz")
combobox_dacFreq.set("1 Hz")

button_setAdcFreq = ttk.Button(master=frameCtl, text="Set ADC Frequency", command=_set_adc_frequency)
button_setDacFreq = ttk.Button(master=frameCtl, text="Set DAC Frequency", command=_set_dac_frequency)
# positioning left
combobox_port.pack(side=LEFT, padx=2)
button_open.pack(side=LEFT, padx=2)
combobox_adcFreq.pack(side=LEFT, padx=2)
button_setAdcFreq.pack(side=LEFT, padx=2)
combobox_dacFreq.pack(side=LEFT, padx=2)
button_setDacFreq.pack(side=LEFT, padx=2)

# positioning right
button_quit.pack(side=RIGHT)
button_clear.pack(side=RIGHT)

# find available port
stmPort = None
ports = serial.tools.list_ports.comports()
portList: List[Any] = []
for port, desc, hwid in sorted(ports):
    portList.append(port)
    if desc.find("STLink") != -1:
        print("{}: {} [{}]".format(port, desc, hwid))
        stmPort = port

# add ports name to combobox list
combobox_port['values'] = portList
if stmPort is not None:
    combobox_port.set(stmPort)

#server hhtp
sse_socket_list: List[Any] = []
webserver_init()

root.mainloop()

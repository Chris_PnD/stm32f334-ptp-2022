/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define DATA_BUFFER_SIZE 		60
#define CIRCULAR_BUFFER_SIZE 	10
#define UART_RX_BUFFER_SIZE 	30
#define MAX_COMMAND_LEN			10
#define DAC_DMA_BUFFER_SIZE     640
#define ADC_DMA_BUFFER_SIZE     20

#define ADC_12BITS_MAX_VALUE	0xFFF

/* Math definitions */
#define _2PI            6.283185307f
#define _PI             3.14159265f

// SLIP ecape character definition
#define SLIP_END		0x0A  /* '/n' */
#define SLIP_ESC		0xDB
#define SLIP_ESC_END	0xDC
#define SLIP_ESC_ESC	0xDD

#define HCLK_CLOCK_HZ	64000000

typedef enum {
	PRINT_MESSAGE_ID,
}messageId_t;

typedef enum {
	STRING_TX,
	BIN_SLIP_TX,
	BIN_PACKET_TX,
} comunicationType_t;

typedef struct dataBuff {
	messageId_t messId;
	uint8_t count;
	char buff[DATA_BUFFER_SIZE];
} dataBuff_t;

typedef struct circularBuff {
	dataBuff_t 	data[CIRCULAR_BUFFER_SIZE];

	uint8_t 	head;
	uint8_t 	tail;
	bool 		full;
	bool		empty;
} circularBuff_t;

typedef struct commandTable_s {
	const char* commandString;
	void(*commandCallback_t)(double var);
} commandTable_t;

typedef struct __attribute__((packed)){
	const char 	id[2];
	double 		epoch;
	float		val;
} binSLIPFormatData_t;

typedef struct {
	comunicationType_t 	txType;
	uint8_t 			adcNbOfSamples;
	uint16_t			freq;
} comTypeControlBlock_t;

typedef struct {
	uint8_t bytes[3];
} tripleBytes_t;

typedef struct __attribute__((packed)){
	const char 		id[2];
	uint8_t     	numberOfPackedData;
	const char  	endOfLine;
	uint16_t 		periodOfSample;
	double			epoch;
	tripleBytes_t 	adcDataPacked[ADC_DMA_BUFFER_SIZE / 2];
} binPacketFormatData_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
volatile uint8_t SR_reg;               /* Current value of the FAULTMASK register */
volatile uint8_t SR_lock = 0x00U;      /* Lock */

/* Save status register and disable interrupts */
#define EnterCritical() \
  do {\
    if (++SR_lock == 1u) {\
      /*lint -save  -e586 -e950 Disable MISRA rule (2.1,1.1) checking. */\
      asm ( \
      "MRS R0, PRIMASK\n\t" \
      "CPSID i\n\t"            \
      "STRB R0, %[output]"  \
      : [output] "=m" (SR_reg)\
      :: "r0");\
      /*lint -restore Enable MISRA rule (2.1,1.1) checking. */\
  }\
} while(0)

/* Restore status register  */
#define ExitCritical() \
  do {\
    if (--SR_lock == 0u) { \
      /*lint -save  -e586 -e950 Disable MISRA rule (2.1,1.1) checking. */\
      asm (                 \
      "ldrb r0, %[input]\n\t"\
      "msr PRIMASK,r0;\n\t" \
      ::[input] "m" (SR_reg)  \
      : "r0");                \
      /*lint -restore Enable MISRA rule (2.1,1.1) checking. */\
    }\
  } while(0)
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

DAC_HandleTypeDef hdac1;
DMA_HandleTypeDef hdma_dac1_ch1;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;
DMA_HandleTypeDef hdma_usart2_rx;

/* USER CODE BEGIN PV */
void cmdTimeCallback(double var);
void cmdAdcFreqCallback(double var);
void cmdDacFreqCallback(double var);

circularBuff_t messages = {
		.empty = true,
};

static const char btMessage[] = "BT message";
static const char timerMessage[] = "Timer message\r\n";
static const char dataMessage[] = "Data:";

static const commandTable_t commandTable[] = {
		{	.commandString = "time", 	.commandCallback_t = cmdTimeCallback,},
		{	.commandString = "adcf",	.commandCallback_t = cmdAdcFreqCallback,},
		{	.commandString = "dacf",	.commandCallback_t = cmdDacFreqCallback,},
};

// uart rx buffer
uint8_t uartRxBuff[UART_RX_BUFFER_SIZE] = {0};
uint8_t *pUartRxBuff = uartRxBuff;

// tx complete flag
volatile bool uartTXCompleteFlag = true;

comTypeControlBlock_t comTypeCb = {
		.txType = STRING_TX,
		.adcNbOfSamples = 1,
		.freq = 1,
};

// dac dma buffer
uint16_t dacDmaBuff[DAC_DMA_BUFFER_SIZE] = {0};

// adc dma buffer
uint16_t adcDmaBuff[ADC_DMA_BUFFER_SIZE] = {0};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_DMA_Init(void);
static void MX_RTC_Init(void);
static void MX_ADC1_Init(void);
static void MX_DAC1_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM7_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
bool addMessage(char *buff, uint16_t size)
{
	EnterCritical();

	if (messages.full == false && buff != NULL)
	{
		if (size > DATA_BUFFER_SIZE)
		{
			size = DATA_BUFFER_SIZE;
		}
		messages.data[messages.head].count = size;
		memcpy(messages.data[messages.head].buff, buff, size);

		// move head ahead
		messages.head = (messages.head + 1) % CIRCULAR_BUFFER_SIZE;

		// empty false
		messages.empty = false;

		//full check
		messages.full = (messages.head == messages.tail);

		ExitCritical();
		return true;
	}

	ExitCritical();
	return false;
}

static void updateRTC(time_t now)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	struct tm time_tm;

    setenv("TZ", "CET-1CEST", 1);
    tzset();
	time_tm = *(localtime(&now));

	sTime.Hours = (uint8_t)time_tm.tm_hour;
	sTime.Minutes = (uint8_t)time_tm.tm_min;
	sTime.Seconds = (uint8_t)time_tm.tm_sec;
	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
	{
		addMessage("SetTime error\r", 15);
	}

	if (time_tm.tm_wday == 0) { time_tm.tm_wday = 7; } // the chip goes mon tue wed thu fri sat sun
	sDate.WeekDay = (uint8_t)time_tm.tm_wday;
	sDate.Month = (uint8_t)time_tm.tm_mon+1; //momth 1- This is why date math is frustrating.
	sDate.Date = (uint8_t)time_tm.tm_mday;
	sDate.Year = (uint16_t)(time_tm.tm_year+1900-2000); // time.h is years since 1900, chip is years since 2000

	/*
	* update the RTC
	*/
	if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
	{
		addMessage("SetDate error\r", 15);
	}
}

bool getMessage(dataBuff_t* data)
{
	EnterCritical();

	if (messages.empty == false && data != NULL)
	{
		data->count = messages.data[messages.tail].count;
		memcpy(data->buff, messages.data[messages.tail].buff, data->count);

		// move tail ahead
		messages.tail = (messages.tail + 1) % CIRCULAR_BUFFER_SIZE;

		// full false
		messages.full = false;

		//full check
		messages.empty = (messages.head == messages.tail);

		ExitCritical();
		return true;
	}

	ExitCritical();
	return false;
}

void cmdTimeCallback(double var)
{
	time_t currentTime = var;
	updateRTC(currentTime);
}


void setTxType(uint16_t freq)
{
	if (freq <= 100) // mode for adc sampling <= 100HZ
	{
		comTypeCb.txType = STRING_TX;
		comTypeCb.adcNbOfSamples = 1;
	}
	else if (freq <= 200) // mode for adc 100Hz < sampling <= 200HZ
	{
		comTypeCb.txType = BIN_SLIP_TX;
		comTypeCb.adcNbOfSamples = 1;
	}
	else // mode for adc sampling > 200HZ
	{
		comTypeCb.txType = BIN_PACKET_TX;
		comTypeCb.adcNbOfSamples = ADC_DMA_BUFFER_SIZE;
	}

	comTypeCb.freq = freq;

	if (HAL_ADC_Stop_DMA(&hadc1) == HAL_OK)
	{
		HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adcDmaBuff, comTypeCb.adcNbOfSamples);
	}
	else
	{
		HAL_DMA_Abort(&hdma_adc1);
	}
}

void cmdAdcFreqCallback(double var)
{
	uint32_t prescaler = htim6.Init.Prescaler + 1;
	uint32_t period = htim6.Init.Period + 1;

	do {
		period = (HCLK_CLOCK_HZ / (prescaler) / round(var));
		if (period < 2)
			prescaler = prescaler  / 10;
		if (period > 0xFFFF)
			prescaler = prescaler  * 10;
	}
	while (period < 2 || prescaler > 0xFFFF);

	htim6.Init.Period = period - 1;
	htim6.Init.Prescaler = prescaler - 1;
	setTxType(HCLK_CLOCK_HZ / prescaler / period);

	TIM_Base_SetConfig(htim6.Instance, &htim6.Init);
}

void cmdDacFreqCallback(double var)
{
	htim7.Init.Period = (1000 / round(var)) - 1;
	TIM_Base_SetConfig(htim7.Instance, &htim7.Init);
}

void ackCmd(char* cmd, double val)
{
	char buff[MAX_COMMAND_LEN * 2];
	sprintf(buff, "ACK:%s=%d\n", cmd, (int)val);
	addMessage(buff, strlen(buff));
}

void commandParser(uint8_t *buffer)
{
	double val;
	char command[MAX_COMMAND_LEN];

	if (sscanf((char *)buffer, "%s %lf", command, &val) == 2)
	{
		for (uint8_t i = 0; i < (sizeof(commandTable) / sizeof(commandTable[0])); i++)
		{
			if (memcmp(commandTable[i].commandString, command, strlen(commandTable[i].commandString)) == 0)
			{
				// command found
				if (commandTable[i].commandCallback_t != NULL)
				{
					commandTable[i].commandCallback_t(val);
					ackCmd(command, val);
				}
			}
		}
	}
}

static uint16_t cmdSetSineWaveformCb(uint16_t i)
{
	// calculating sine wave
	return (0.95F * sinf(_2PI * ((float)i / DAC_DMA_BUFFER_SIZE)) + 1) * ADC_12BITS_MAX_VALUE / 2;
}

static void dacBuffInit(uint16_t *dacDmaBuff)
{
	for(uint16_t i = 0; i < DAC_DMA_BUFFER_SIZE; i++)
	{
		dacDmaBuff[i] = cmdSetSineWaveformCb(i);
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	char buff[DATA_BUFFER_SIZE];

	sprintf(buff,"Button pressed\r");
	// create message
	addMessage((char *)buff, strlen(buff));
}

time_t time2epoch(RTC_TimeTypeDef* currentTime, RTC_DateTypeDef* currentDate)
{
	struct tm currTime;

	currTime.tm_year = currentDate->Year + 100;  // In fact: 2000 + 18 - 1900
	currTime.tm_mday = currentDate->Date;
	currTime.tm_mon  = currentDate->Month - 1;

	currTime.tm_hour = currentTime->Hours;
	currTime.tm_min  = currentTime->Minutes;
	currTime.tm_sec  = currentTime->Seconds;

	return mktime(&currTime);
}

uint16_t slipEncode(char *dataBufferOut, uint16_t outSize, char *dataBufferIn, uint16_t inSize)
{
	char* bufferOutPt = dataBufferOut;

	for (uint16_t i = 0; i < inSize; i++)
	{
		switch (dataBufferIn[i])
		{
		case SLIP_END:
			*(bufferOutPt++) = SLIP_ESC;
			*(bufferOutPt++) = SLIP_ESC_END;
		break;
		case SLIP_ESC:
			*(bufferOutPt++) = SLIP_ESC;
			*(bufferOutPt++) = SLIP_ESC_ESC;
		break;
		default:
			*(bufferOutPt++) = dataBufferIn[i];
		}
	}
	*(bufferOutPt++) = SLIP_END;
	return (bufferOutPt - dataBufferOut);
}

void data12bitsPacker(tripleBytes_t *dataBuffOut, uint16_t *dataBuffIn, uint8_t sizeOfIn)
{
	for (uint8_t i = 0; i < sizeOfIn; i+=2)
	{
		dataBuffOut->bytes[0] = dataBuffIn[i] >> 4;
		dataBuffOut->bytes[1] = ((dataBuffIn[i] << 4) & 0xF0) | (dataBuffIn[i + 1] >> 8);
		dataBuffOut->bytes[2] = dataBuffIn[i + 1];
		dataBuffOut++;
	}
}

void adcCompleteCallback(ADC_HandleTypeDef *hadc)
{
	RTC_TimeTypeDef rtcTime;
	RTC_DateTypeDef rtcDate;

	HAL_RTC_GetTime(&hrtc, &rtcTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &rtcDate, RTC_FORMAT_BIN);

	uint16_t time_ms = (1000 * (rtcTime.SecondFraction - rtcTime.SubSeconds)) / (rtcTime.SecondFraction + 1);

	// create message depend on transmission type

	if (comTypeCb.txType == STRING_TX)
	{
		char buff_st[DATA_BUFFER_SIZE];

		float adcValue = 3.3F * adcDmaBuff[0] / ADC_12BITS_MAX_VALUE;

		uint16_t size = sprintf(buff_st, "%s %02d:%02d:%02d.%03d %.2f\n", dataMessage, rtcTime.Hours, rtcTime.Minutes, rtcTime.Seconds, time_ms, adcValue);
		addMessage((char *)buff_st, size);
	}
	else if (comTypeCb.txType == BIN_SLIP_TX)
	{
		binSLIPFormatData_t buff_binSLIP = {
				.id = "B:",
		};
		char dataBufferOut[2* sizeof(buff_binSLIP)];

		float adcValue = 3.3F * adcDmaBuff[0] / ADC_12BITS_MAX_VALUE;

		buff_binSLIP.epoch = time2epoch(&rtcTime, &rtcDate) + (double)time_ms / 1000;
		buff_binSLIP.val = adcValue;
		// SLIP encode
		uint16_t size = slipEncode(dataBufferOut, sizeof(dataBufferOut), (char *)&buff_binSLIP, sizeof(buff_binSLIP));
		addMessage(dataBufferOut, size);
	}
	else if (comTypeCb.txType == BIN_PACKET_TX)
	{
		// buffer of adc data with size ADC_DMA_BUFFER_SIZE (uint16_t)
		binPacketFormatData_t buff_binPacket = {
				.id = "P:",
				.endOfLine = '\n',
		};
		buff_binPacket.epoch = time2epoch(&rtcTime, &rtcDate) + (double)time_ms / 1000;
		buff_binPacket.numberOfPackedData = sizeof(buff_binPacket.periodOfSample) +
											sizeof(buff_binPacket.epoch) +
											sizeof(buff_binPacket.adcDataPacked);
		buff_binPacket.periodOfSample = comTypeCb.freq;
		//buff_binPacket.adcDataPacked <<== adcDmaBuff[ADC_DMA_BUFFER_SIZE]
		data12bitsPacker(buff_binPacket.adcDataPacked, adcDmaBuff, ADC_DMA_BUFFER_SIZE);
		addMessage((char*)&buff_binPacket, sizeof(buff_binPacket));
	}

	HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
}

void uartRxCallback(struct __UART_HandleTypeDef *huart, uint16_t Pos)
{
	if (uartRxBuff[Pos - 1] == '\r')
	{
		commandParser(uartRxBuff);
	}

	HAL_UARTEx_ReceiveToIdle_DMA(&huart2, uartRxBuff, UART_RX_BUFFER_SIZE);
}

void uartTxCallback(UART_HandleTypeDef *huart)
{
	uartTXCompleteFlag = true;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  // unused variable workaround
  (void)timerMessage;
  (void)btMessage;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  MX_DMA_Init();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_RTC_Init();
  MX_ADC1_Init();
  MX_DAC1_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */
  // register DMA UART tx interrupt
  HAL_UART_RegisterCallback(&huart2, HAL_UART_TX_COMPLETE_CB_ID, uartTxCallback);

  // timer 6 start trigger ADC
  HAL_TIM_Base_Start(&htim6);

  // timer 7 start trigger DAC
  HAL_TIM_Base_Start(&htim7);

  // adc config
  HAL_ADC_RegisterCallback(&hadc1, HAL_ADC_CONVERSION_COMPLETE_CB_ID, adcCompleteCallback);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adcDmaBuff, comTypeCb.adcNbOfSamples);

  //dac config
  dacBuffInit(dacDmaBuff);
  HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t *)dacDmaBuff, sizeof(dacDmaBuff) / sizeof(dacDmaBuff[0]), DAC_ALIGN_12B_R);

  // uart2 register callback
  HAL_UART_RegisterRxEventCallback(&huart2, uartRxCallback);
  HAL_UARTEx_ReceiveToIdle_DMA(&huart2, uartRxBuff, UART_RX_BUFFER_SIZE);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  dataBuff_t message;

	  // check message
	  if (uartTXCompleteFlag == true && getMessage(&message) == true)
	  {
		  uartTXCompleteFlag = false;
		  HAL_UART_Transmit_DMA(&huart2, (uint8_t *)message.buff, message.count);
	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T6_TRGO;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief DAC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC1_Init(void)
{

  /* USER CODE BEGIN DAC1_Init 0 */

  /* USER CODE END DAC1_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC1_Init 1 */

  /* USER CODE END DAC1_Init 1 */

  /** DAC Initialization
  */
  hdac1.Instance = DAC1;
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    Error_Handler();
  }

  /** DAC channel OUT1 config
  */
  sConfig.DAC_Trigger = DAC_TRIGGER_T7_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC1_Init 2 */

  /* USER CODE END DAC1_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 0;
  hrtc.Init.SynchPrediv = 32767;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 64000 - 1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 1000 -1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 100 - 1;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 1000 - 1;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
